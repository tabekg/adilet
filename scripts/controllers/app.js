app.controller('AppCtrl', ["$rootScope", "$scope", "$cookies", function($rootScope, $scope, $cookies){
	$rootScope.current = {
    language: $cookies.get('language')
  };

  if ($rootScope.current.language != 'ky' && $rootScope.current.language != 'ru' && $rootScope.current.language != 'en'){
    $rootScope.language = $rootScope.languages.en;
		$rootScope.current.language = 'en';
  } else {
    $rootScope.language = $rootScope.languages[$rootScope.current.language];
  }

  $rootScope.changeLanguage = function(newLanguage){
		var expireDate = new Date(),
				params = {};
  	expireDate.setDate(expireDate.getYear() + 1);
		params.expires = expireDate;
    if (newLanguage == 'ru') {
			$rootScope.language = $rootScope.languages.ru;
			$cookies.put('language', 'ru', params);
			$rootScope.current.language = 'ru';
		} else if (newLanguage == 'en') {
			$rootScope.language = $rootScope.languages.en;
			$cookies.put('language', 'en', params);
			$rootScope.current.language = 'en';
		} else {
			$rootScope.language = $rootScope.languages.ky;
			$cookies.put('language', 'ky', params);
			$rootScope.current.language = 'ky';
		}
  }
}]);
