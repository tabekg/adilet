(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.tooltipped').tooltip({delay: 50});
  });
})(jQuery);

var app = angular
	.module('adilet', [
		'ui.router',
		'ui.materialize',
    'ngCookies',
    '720kb.tooltips',
    'ngMap'
	])
	.run(["$rootScope", function($rootScope){
    
	}]);
