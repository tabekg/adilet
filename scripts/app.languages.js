app.run(["$rootScope", function($rootScope){
  $rootScope.languages = {
    ky: null,
    ru: null,
    en: null
  };
  $rootScope.languages.ky = {
    app: {
      name: 'Сулайман-Тоо'
    },
    home: 'Башкы бет',
    about: 'Маалымат',
    pictures: 'Сүрөттөрү',
    history: 'Тарыхы',
    geography: 'Географиялык абалы'
  }
  $rootScope.languages.ru = {
    app: {
      name: 'Сулайма́н-То́о'
    },
    home: 'Главная',
    about: 'О горах',
    pictures: 'Фотографии',
    history: 'История',
    geography: 'Географическое положение'
  }
  $rootScope.languages.en = {
    app: {
      name: 'Sulayman-Too'
    },
    home: 'Home',
    about: 'About',
    pictures: 'Pictures',
    history: 'History',
    geography: 'Geography'
  }
}]);
