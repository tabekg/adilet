app.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider
    .otherwise('/app.index');
  $stateProvider
    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'views/app.html'
    })
    .state('app.index', {
      url: '.index',
      templateUrl: 'views/index.html',
      controller: 'IndexCtrl'
    })
    .state('app.about', {
      url: '.about',
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl'
    })
    .state('app.pictures', {
      url: '.pictures',
      templateUrl: 'views/pictures.html',
      controller: 'PicturesCtrl'
    })
    .state('app.history', {
      url: '.history',
      templateUrl: 'views/history.html',
      controller: 'HistoryCtrl'
    })
    .state('app.geography', {
      url: '.geography',
      templateUrl: 'views/geography.html',
      controller: 'GeographyCtrl'
    });
}]);
